const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const auth = require("../auth");

//Create order
module.exports.createOrder = (data) =>{
    return User.findById(data.userId).then((result) => {
    if (result.isAdmin === true){
        return false;
    } else {
        return Product.findById(data.productId).then((result, err) => {
            let price = Product.findById(data.productId).then(result=>{
                return result.price;
            })
            let newOrder = new Order({
                userId: data.userId,
                totalAmount: data.quantity*result.price,
                products: [{
                    productId: data.productId,
                    quantity: data.quantity,
                    }]
                 })
                   return newOrder.save().then((product,error)=>{
                       if(error){
                        return false;
                        }else {
                            return Order.findOne({userId:data.userId}).then((order,error)=>{
                              console.log(data);
                                let orderData = {
                                    orders: [{
                                        orderId: order._id,
                                        quantity: data.quantity
                                    }]
                                }
                                return Product.findByIdAndUpdate(data.productId, {$push: orderData}).then((order,error)=>{
                                    if (error){
                                        return false;
                                    }else{
                                        return "Order Successful!";
                                    }
                                })
                            })
                            }
                })
            })
        }
        })
}